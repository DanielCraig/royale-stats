import React, { Component } from 'react'
import { Segment, Container, Popup, Icon, Divider, Item, Header, Grid, Image, Label, Table } from 'semantic-ui-react'

export default class PlayerTable extends Component {
    render() {

        const { playerInfo, playerStats, playerArena, playerClan, playerGames, arenaImage } = this.props

        const styleImg = {
            display: "inline-block",
            width: "140px",
            maxWidth: "140px",
            height: "auto",
            verticalAlign: "middle",
            boxSizing: "inherit",
            position: "relative"
        }

        const divImg = {
            float: "right",
            textAlign: "center",
            width: "140px",
            position: "relative",
            maxWidth: "140px",
            boxSizing: "inherit",
            display: "block",
        }

        return (
            <div hidden={this.props.hidden}>
                <Container style={{ marginTop: "20px", boxShadow: "17px 7px 27px -13px rgba(0,0,0,0.55)" }}>
                    <Segment style={{ marginLeft: "auto", marginRight: "auto", display: "block", maxWidth: "100%", boxSizing: "inherit", fontFamily: "'Source Sans Pro','Helvetica Neue',Arial,Helvetica,sans-serif", fontSize: "14px" }}>
                        <div style={divImg}>
                            <img className="small" style={styleImg} alt="arena" src={arenaImage}></img>
                        </div>
                        <div style={{ display: "flex" }}>
                            <div style={{ display: "block", marginRight: "1rem", boxSizing: "inherit" }}>
                                <h1 style={{ margin: "0", whiteSpace: "nowrap", fontSize: "1.5rem", fontFamily: "'Source Sans Pro','Helvetica Neue',Arial,Helvetica,sans-serif" }} className=""> {playerInfo.name} </h1>
                                <h2 style={{ margin: "0", fontFamily: "'Source Sans Pro','Helvetica Neue',Arial,Helvetica,sans-serif", fontSize: ".85714286em" }}> #{playerInfo.tag} </h2>
                            </div>
                            <div style={{ display: "block", color: "#e03997" }}>
                                <div>
                                    <Popup
                                        trigger={<Icon name="heart outline" color="red" circular />}
                                        content="Follow player"
                                        position="top center"
                                    />
                                </div>
                            </div>
                        </div>
                        <Divider hidden style={{ margin: "0.1rem" }} />
                        <div floated="left" style={{ position: "absolute", paddingTop: "10px" }}>
                            <Item style={{ fontFamily: "'Source Sans Pro','Helvetica Neue',Arial,Helvetica,sans-serif", display: "inline-block" }}> {playerInfo.trophies} / {playerStats.maxTrophies}<img alt="trophy" style={{ marginLeft: "0.5rem", width: "1.4rem", height: "auto" }} src="https://royaleapi.com/static/img/ui/trophy.png" /> &nbsp;| </Item>
                            <Item style={{ display: "inline-block" }}> &nbsp;{playerArena.arena} | </Item>
                            <Item style={{ display: "inline-block" }}> &nbsp;{playerArena.name} </Item>
                        </div>
                        <Divider hidden style={{ margin: "0.1rem" }} />
                        <div>
                            <Item style={{ paddingTop: "40px", maxWidth: "40px" }}>
                                <a href="https://royaleapi.com/clan/V9Q09R9" className="image">
                                    <img alt="clanImage" className="image" src={playerClan.image} style={{ width: "35px", height: "auto", display: "block" }}></img>
                                </a>
                            </Item>
                        </div>
                        <br></br>
                        <Divider></Divider>
                        <Container>
                            <Segment>
                                <div>
                                    <Header as="h3" floated="left" style={{ fontSize: "1.28571429rem", fontWeight: "600" }}>Chests</Header>
                                </div>
                                <br></br>
                                <div style={{ marginTop: "1rem" }}>
                                    <Grid columns={8} padded>
                                        <Grid.Row>
                                            <Grid.Column>
                                                <div style={{ position: "relative", display: "inline-block", paddingRight: "1rem", verticalAlign: "top", maxWidth: "140px" }}>
                                                    <Image alt="Golden Chest" src="https://royaleapi.com/static/img/chests/chest-golden.png?t=1544082990.2094533"></Image>
                                                    <Label circular size="small" color="red" floating>+1</Label>
                                                    <p>Golden Chest</p>
                                                </div>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <div style={{ position: "relative", display: "inline-block", paddingRight: "1rem", verticalAlign: "top", maxWidth: "140px" }}>
                                                    <Image alt="Silver Chest" src="https://royaleapi.com/static/img/chests/chest-silver.png?t=1544082990.2094533"></Image>
                                                    <Label circular size="small" color="red" floating>+2</Label>
                                                    <p>Silver Chest</p>
                                                </div>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <div style={{ position: "relative", display: "inline-block", paddingRight: "1rem", verticalAlign: "top", maxWidth: "140px" }}>
                                                    <Image alt="Silver Chest" src="https://royaleapi.com/static/img/chests/chest-silver.png?t=1544082990.2094533"></Image>
                                                    <Label circular size="small" color="red" floating>+3</Label>
                                                    <p>Silver Chest</p>
                                                </div>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <div style={{ position: "relative", display: "inline-block", paddingRight: "1rem", verticalAlign: "top", maxWidth: "140px" }}>
                                                    <Image alt="Silver Chest" src="https://royaleapi.com/static/img/chests/chest-silver.png?t=1544082990.2094533"></Image>
                                                    <Label circular size="small" color="red" floating>+4</Label>
                                                    <p>Silver Chest</p>
                                                </div>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <div style={{ position: "relative", display: "inline-block", paddingRight: "1rem", verticalAlign: "top", maxWidth: "140px" }}>
                                                    <Image alt="Silver Chest" src="https://royaleapi.com/static/img/chests/chest-silver.png?t=1544082990.2094533"></Image>
                                                    <Label circular size="small" color="red" floating>+5</Label>
                                                    <p>Silver Chest</p>
                                                </div>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <div style={{ position: "relative", display: "inline-block", paddingRight: "1rem", verticalAlign: "top", maxWidth: "140px" }}>
                                                    <Image alt="Giant Chest" src="https://royaleapi.com/static/img/chests/chest-giant.png?t=1544082990.2054534"></Image>
                                                    <Label circular size="small" color="red" floating>+6</Label>
                                                    <p>Giant Chest</p>
                                                </div>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <div style={{ position: "relative", display: "inline-block", paddingRight: "1rem", verticalAlign: "top", maxWidth: "140px" }}>
                                                    <Image alt="Silver Chest" src="https://royaleapi.com/static/img/chests/chest-silver.png?t=1544082990.2094533"></Image>
                                                    <Label circular size="small" color="red" floating>+7</Label>
                                                    <p>Silver Chest</p>
                                                </div>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <div style={{ position: "relative", display: "inline-block", paddingRight: "1rem", verticalAlign: "top", maxWidth: "140px" }}>
                                                    <Image alt="Silver Chest" src="https://royaleapi.com/static/img/chests/chest-silver.png?t=1544082990.2094533"></Image>
                                                    <Label circular size="small" color="red" floating>+8</Label>
                                                    <p>Silver Chest</p>
                                                </div>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <div style={{ position: "relative", display: "inline-block", paddingRight: "1rem", marginTop: "2rem", verticalAlign: "top", maxWidth: "140px" }}>
                                                    <Image alt="Silver Chest" src="https://royaleapi.com/static/img/chests/chest-silver.png?t=1544082990.2094533"></Image>
                                                    <Label circular size="small" color="red" floating>+9</Label>
                                                    <p>Silver Chest</p>
                                                </div>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <div style={{ position: "relative", display: "inline-block", paddingRight: "1rem", marginTop: "2rem", verticalAlign: "top", maxWidth: "140px" }}>
                                                    <Image alt="Magical Chest" src="https://royaleapi.com/static/img/chests/chest-magical.png?t=1544082990.2094533"></Image>
                                                    <Label circular size="small" color="red" floating>+19</Label>
                                                    <p size="small">Magical Chest</p>
                                                </div>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <div style={{ position: "relative", display: "inline-block", paddingRight: "1rem", marginTop: "2rem", verticalAlign: "top", maxWidth: "140px" }}>
                                                    <Image alt="Mega Lightning Chest" src="https://royaleapi.com/static/img/chests/chest-megalightning.png?t=1544082990.2094533"></Image>
                                                    <Label circular size="small" color="red" floating>+53</Label>
                                                    <p>Mega Lightning Chest</p>
                                                </div>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <div style={{ position: "relative", display: "inline-block", paddingRight: "1rem", marginTop: "2rem", verticalAlign: "top", maxWidth: "140px" }}>
                                                    <Image alt="Legendary Chest" src="https://royaleapi.com/static/img/chests/chest-legendary.png?t=1544082990.2094533"></Image>
                                                    <Label circular size="small" color="red" floating>+227</Label>
                                                    <p>Legendary Chest</p>
                                                </div>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <div style={{ position: "relative", display: "inline-block", paddingRight: "1rem", marginTop: "2rem", verticalAlign: "top", maxWidth: "140px" }}>
                                                    <Image alt="Epic Chest" src="https://royaleapi.com/static/img/chests/chest-epic.png?t=1544082990.2054534"></Image>
                                                    <Label circular size="small" color="red" floating>+323</Label>
                                                    <p>Epic Chest</p>
                                                </div>
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>
                                </div>
                            </Segment>
                        </Container>
                        <Divider></Divider>
                        <Segment>
                            <Grid relaxed divided columns={2}>
                                <Grid.Column style={{ paddingTop: "0 !important" }}>
                                    <Container>
                                        <Image src="https://royaleapi.com/static/img/ui/battle.png" floated="right" size="mini"></Image>
                                        <Header as="h3" floated="left">Battle Stats</Header>
                                        <Table singleLine compact basic style={{ border: "none", fontSize: "13px" }}>
                                            <Table.Row>
                                                <Table.Cell width={2}>
                                                    <Header as="h5">Clan Wars</Header>
                                                </Table.Cell>
                                            </Table.Row>
                                            <Table.Row>
                                                <Table.Cell > Clan Cards Collected</Table.Cell>
                                                <Table.Cell textAlign="right">{playerStats.clanCardsCollected}</Table.Cell>
                                            </Table.Row>
                                            <Table.Row>
                                                <Table.Cell >War Day Wins</Table.Cell>
                                                <Table.Cell textAlign="right">{playerGames.warDayWins}</Table.Cell>
                                            </Table.Row>
                                        </Table>
                                        <Table singleLine compact basic style={{ border: "none", fontSize: "13px" }}>
                                            <Table.Header>
                                                <Table.Cell>
                                                    <Header style={{ fontSize: "13px" }}>Ladder + Challenges</Header>
                                                </Table.Cell>
                                                <Table.Cell textAlign="right">
                                                    <Header style={{ fontSize: "13px" }}>Count</Header>
                                                </Table.Cell>
                                            </Table.Header>
                                            <Table.Row >
                                                <Table.Cell >
                                                    <Header style={{ fontSize: "13px" }}>Wins</Header>
                                                </Table.Cell>
                                                <Table.Cell textAlign="right">
                                                    {playerGames.wins}
                                                </Table.Cell>
                                            </Table.Row>
                                            <Table.Row>
                                                <Table.Cell>
                                                    <Header style={{ fontSize: "13px" }}>Losses</Header>
                                                </Table.Cell>
                                                <Table.Cell textAlign="right">
                                                    {playerGames.losses}
                                                </Table.Cell>
                                            </Table.Row>
                                        </Table>
                                        <Table singleLine compact basic style={{ border: "none", fontSize: "13px" }}>
                                            <Table.Row>
                                                <Table.Cell>
                                                    <Header style={{ fontSize: "13px" }}>1v1 draws<Header style={{ fontSize: "13px", textTransform: "lowercase" }} sub>and 2v2 games</Header></Header>
                                                </Table.Cell>
                                                <Table.Cell textAlign="right">
                                                    {playerGames.draws}
                                                </Table.Cell>
                                            </Table.Row>
                                            <Table.Row>
                                                <Table.Cell>
                                                    <Header style={{ fontSize: "13px" }}> Total Games</Header>
                                                </Table.Cell>
                                                <Table.Cell textAlign="right">
                                                    {playerGames.total}
                                                </Table.Cell>
                                            </Table.Row>
                                            <Table.Row>
                                                <Table.Cell>
                                                    <Header style={{ fontSize: "13px" }}>Three crown wins</Header>
                                                </Table.Cell>
                                                <Table.Cell textAlign="right">
                                                    {playerStats.threeCrownWins}
                                                </Table.Cell>
                                            </Table.Row>
                                        </Table>
                                    </Container>
                                </Grid.Column>
                                <Grid.Column style={{ paddingTop: "0 !important" }}>
                                    <Container>
                                        <Image src="https://royaleapi.com/static/img/ui/tournament.png" floated="right" size="mini"></Image>
                                        <Header as="h3" floated="left">Challenge Stats</Header>
                                    </Container>
                                    <Table singleLine compact basic style={{ border: "none", fontSize: "13px" }}>
                                        <Table.Row>
                                            <Table.Cell width={2}>
                                                <Header as="h5">Challenges</Header>
                                            </Table.Cell>
                                        </Table.Row>
                                        <Table.Row>
                                            <Table.Cell>Max Wins</Table.Cell>
                                            <Table.Cell textAlign="right"> {playerStats.challengeMaxWins} </Table.Cell>
                                        </Table.Row>
                                        <Table.Row>
                                            <Table.Cell>Cards Won</Table.Cell>
                                            <Table.Cell textAlign="right"> {playerStats.challengeCardsWon} </Table.Cell>
                                        </Table.Row>
                                        <Table.Row>
                                            <Table.Cell>
                                                <Header style={{ fontSize: "13px" }}>Tournaments</Header>
                                            </Table.Cell>
                                        </Table.Row>
                                        <Table.Row>
                                            <Table.Cell>Total Games</Table.Cell>
                                            <Table.Cell textAlign="right"> {playerGames.tournamentGames} </Table.Cell>
                                        </Table.Row>
                                        <Table.Row>
                                            <Table.Cell>Cards Won</Table.Cell>
                                            <Table.Cell textAlign="right"> {playerStats.tournamentCardsWon} </Table.Cell>
                                        </Table.Row>
                                    </Table>
                                </Grid.Column>
                                <Grid.Column style={{ paddingTop: "0 !important" }}>
                                    <Container>
                                        <Image src="https://royaleapi.com/static/img/ui/cards.png" floated="right" size="mini"></Image>
                                        <Header as="h3" floated="left">Misc Stats</Header>
                                        <Table singleLine compact basic style={{ border: "none", fontSize: "13px" }}>
                                            <Table.Row>
                                                <Table.Cell>Experience</Table.Cell>
                                                <Table.Cell textAlign="right">Level {playerStats.level}</Table.Cell>
                                            </Table.Row>
                                            <Table.Row>
                                                <Table.Cell>Cards Found</Table.Cell>
                                                <Table.Cell textAlign="right">{playerStats.cardsFound} {" "}/{" "}94</Table.Cell>
                                            </Table.Row>
                                            <Table.Row>
                                                <Table.Cell>Total Donations</Table.Cell>
                                                <Table.Cell textAlign="right">{playerStats.totalDonations}</Table.Cell>
                                            </Table.Row>
                                        </Table>
                                    </Container>
                                </Grid.Column>
                            </Grid>
                        </Segment>
                    </Segment>
                </Container >
            </div >
        )
    }
}
