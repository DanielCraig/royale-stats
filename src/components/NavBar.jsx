import React, { Component } from 'react'
import {
    NavbarContainer,
    Navbar,
    NavItem,
    Link
} from 'uikit-react'

export default class NavBar extends Component {
    render() {
        return (
            <div>
                <NavbarContainer>
                    <Navbar left>
                        <NavItem>
                            <Link href="#">Home</Link>
                        </NavItem>
                    </Navbar>
                </NavbarContainer>
            </div>
        )
    }
}
