import React, { Component } from 'react'
import SearchForm from './SearchForm'
import PlayerTable from './playerTable'

export default class MainContent extends Component {

    state = {
        isHidden: true,
        playertag: "",
        playerInfo: "",
        playerStats: "",
        playerGames: "",
        playerDeck: "",
        playerArena: "",
        playerClan: "",
        playerChest: "",
        arenaImage: ""
    }


    handleInput = (event) => {
        this.setState({
            playertag: event.target.value
        })
    }

    toggleHidden = async () => {

        const hidden = this.state.isHidden

        if (hidden === true) {
            let request = {
                headers: new Headers({ 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjczMywiaWRlbiI6IjIwMzkwOTQ5MzI5MjAwNzQyNCIsIm1kIjp7fSwidHMiOjE1NjI3NDMwNzQ3MzR9.50TforqZavWOKddHeWGAv0fDawe6koirwIffOr2lwZQ' })
            }

            await fetch("https://api.royaleapi.com/player/" + this.state.playertag, request).then(response => response.json()).then(data => {

                this.setState({
                    isHidden: !this.state.isHidden
                })

                switch (data.arena.arenaID) {
                    case 1:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena1.png?2" })
                        break
                    case 2:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena2.png?2" })
                        break
                    case 3:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena3.png?2" })
                        break
                    case 4:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena4.png?2" })
                        break
                    case 5:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena5.png?2" })
                        break
                    case 6:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena6.png?2" })
                        break
                    case 7:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena7.png?2" })
                        break
                    case 8:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena8.png?2" })
                        break
                    case 9:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena9.png?2" })
                        break
                    case 10:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena10.png?2" })
                        break
                    case 11:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena11.png?2" })
                        break
                    case 12:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena12.png?2" })
                        break
                    case 13:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena13.png?2" })
                        break
                    case 14:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena14.png?2" })
                        break
                    case 15:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena15.png?2" })
                        break
                    case 16:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena16.png?2" })
                        break
                    case 17:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena17.png?2" })
                        break
                    case 18:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena18.png?2" })
                        break
                    case 19:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena19.png?2" })
                        break
                    case 20:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena20.png?2" })
                        break
                    case 21:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena21.png?2" })
                        break
                    case 22:
                        this.setState({ arenaImage: "https://royaleapi.com/static/img/arenas/arena22.png?2" })
                        break
                    default:
                        this.setState({ arenaImage: "" })
                }

                this.setState({ playerInfo: data, playerStats: data.stats, playerGames: data.games, playerDeck: data.currentDeck, playerArena: data.arena, playerClan: data.clan.badge })
            })

            fetch("https://api.royaleapi.com/player/" + this.state.playertag + "/chests", request).then(response => response.json()).then(data => {
                this.setState({ playerChest: data })
            })
        }
    }


    render() {
        const { playertag, isHidden, playerInfo, playerStats, playerArena, playerClan, playerGames, arenaImage } = this.state
        return (
            <div style={{ display: "flex", minHeight: "19vh", flexDirection: "column" }}>
                <SearchForm playertag={playertag} toggleHidden={this.toggleHidden} handleInput={this.handleInput} />
                <PlayerTable hidden={isHidden} playerInfo={playerInfo} playerStats={playerStats} playerArena={playerArena} playerClan={playerClan} playerGames={playerGames} arenaImage={arenaImage} />
            </div>
        )
    }
}