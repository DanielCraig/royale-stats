import React, { Component } from 'react'

export class SearchForm extends Component {
    render() {
        const { toggleHidden, handleInput } = this.props
        return (
            <div className="uk-container">
                <form>
                    <div className="uk-margin">
                        <input onKeyUp={handleInput} className="uk-input uk-form-width-medium" type="text" placeholder="Enter player tag"></input>
                    </div>
                    <button onClick={() => toggleHidden()} type="button" className="uk-button uk-button-default">Submit</button>
                </form>
            </div>
        )
    }
}

export default SearchForm
