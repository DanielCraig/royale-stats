import React, { Component } from 'react'

export default class Header extends Component {
    render() {

        const styleIcon = {
            borderRadius: "20px",
            boxShadow: "9px 6px 32px -6px rgba(0,0,0,0.68)"
        }
        return (
            <div className="uk-section">
                <img src="https://etgeekera.files.wordpress.com/2016/05/clash-royale-icon.jpg?w=512" style={styleIcon} width="120" height="120" alt="royaleIcon" uk-img></img>
                <h3>Clash Royale Stats</h3>
                <p>View here all the info you need.<br></br> Just enter your player tag which can be found in-game.</p>
            </div>
        )
    }
}
