import React from 'react';
import NavBar from './components/NavBar'
import Header from './components/Header'
import MainContent from './components/MainContent'
import Footer from './components/Footer'
import './App.css';

function App() {
  return (
    <div className="App">
      <NavBar />
      <Header />
      <MainContent />
      <Footer />
    </div>
  );
}

export default App;
